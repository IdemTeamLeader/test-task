'use strict';
const webpack = require('webpack'),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  //UglifyJS = require("uglify-es"),
  //UglifyJSPlugin = require('uglifyjs-webpack-plugin'),
  glob = require("glob"),
  path = require('path'),
  CopyWebpackPlugin = require('copy-webpack-plugin'),
  FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
try {
  require('os').networkInterfaces();
}
catch (e) {
  require('os').networkInterfaces = () => ({});
}
module.exports = function(env) {
  env.production = (env.production==undefined) ? false : true;
  env.development = (env.development==undefined) ? false : true;
  const pluginsOptions = [
    new ExtractTextPlugin({
      filename: './styles/styles.css',
      allChunks: true
    }),
    new webpack.NoEmitOnErrorsPlugin(), //NoEmitOnErrorsPlugin - при возниконовении ошибки файлы не будут создаваться
    new FriendlyErrorsWebpackPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    }),
    new CopyWebpackPlugin([
      {
        from: './img',
        to: './img'
      }, {
        from: './fonts',
        to: './fonts'
      },
    ]),
    
  ];
  
  let pages = glob.sync(__dirname + '/src/*.pug');
  pages.forEach(function (file) {
    
    var base = path.basename(file, '.pug');
    
    pluginsOptions.push(
      new HtmlWebpackPlugin({
        filename: './' + base + '.html',
        template: base + '.pug',
        inject: false
      })
    )
  });
  
  return {
    context: __dirname + '/src/',
    entry : {
      common: './scripts/app.ts',
      //styles: './styles/style.scss'
    }, //Файлы для компиляции
    output: {
      filename: './scripts/[name]'+'.bundle'+'.js', //Результат работы Webpack будет в файле с таким именем
      path: __dirname + '/build/',
      library: '[name]'
    },
    resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: ['.ts', '.tsx', '.js'] // note if using webpack 1 you'd also need a '' in the array as well
    },
    module: {
      rules: [
        {
          test: /\.(sass|scss|css)$/,
          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  importLoaders: 1,
                  minimize: env.production
                }
              },
              {
                loader: 'postcss-loader'
              },
              {
                loader: 'group-css-media-queries-loader',
              },
              {
                loader: 'sass-loader'
              },
            ],
            publicPath:  __dirname + '/source'
          })
        },
        { test: /\.tsx?$/, loader: 'ts-loader' },
        {
          test: /\.pug$/,
          use: [
            {
              loader: 'raw-loader'
            },
            {
              loader: 'pug-html-loader'
            }
          ]
        },
        {
          test: /\.(ttf|otf|eot|svg|woff2|woff?)(\?.+)?$/,
          loader: 'file-loader',
          options:  {
            limit: 10000
          }
        },
      ],
    },
    
    plugins: pluginsOptions,
    watch: env.development, //Как только что-то поменяется, Webpack сам перезапустит сборку
    watchOptions: {
      poll: true
    },
    devServer: {
      contentBase: path.resolve(__dirname, './build'),
      host: '0.0.0.0',
      disableHostCheck: true,
      port: 9001
    }
  };
};